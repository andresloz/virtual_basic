#!/usr/bin/env python
#*- coding: utf-8 -*-

from os import listdir, path, getcwd, sys
from virtualbasic import *

if __name__ == "__main__":
	# example of python code to convert a virtualbasic file
	if len(sys.argv) < 2:
		print "*******************************************************************"
		print " *** VirtualBasic command line tool *******************************"
		print " *** Andres Lozano a.k.a Loz, copyleft, 2011-2015 *****************"
		print " 1) To convert VirtualBasic => Applesoft Basic"
		print "    After choice 1 you can add parameters ex: 1 c remgo"
		print "    c) compact code"
		print "    e) emulator ready"
		print "    u) ultra compact code"
		print "    remgo) add goto, gosub information if possible"
		print " 2) To convert Applesoft Basic => VirtualBasic"
		print " 3) To compact Applesoft Basic code"
		print " 4) To merge in one file all inserts VirtualBasic files"
		print "*******************************************************************"
		s = raw_input("Choose 1, 2, 3 or 4 or type return to exit ? ->")
		choice = s.split()
		if not choice:exit()
		print "You choose",choice[0],"\n"
		if choice[0] == "1" or choice[0] == "4":
			type = "VirtualBasic"
			ext = "*.baz"
		elif choice[0] == "2" or choice[0] == "3":
			type = "AppleSoft Basic"
			ext = "*.bas"
		else:
			exit()
	
		print "*** Define dir and file, actual path is", getcwd(), "***"
		dir = raw_input("Directory path ? ->")
		if path.isdir(dir):
			print "*** files in directory ***"
			for f in listdir(dir):
				print "- ", f
		else:
			print "error path"
			exit()

		print
		fileName = raw_input("File name ? ->")
		fileName = path.join(dir,fileName)		
	else:
		fileName = sys.argv[1]
		if fileName[-3:] == "baz":
			choice = ["1"]
		else:
			choice = ["2"]
			
		if len(sys.argv) > 2:
			choice[1:] = sys.argv[1:]
				
	if fileName:
		# read source file
		try:
			f = open(fileName,"r")
			lines = f.readlines()
			f.close()
		except:
			print "Cannot read file or file not found !"
			print fileName
			exit()
	else:
		exit()
	
	# choice 1) convert virtualbasic to basic, 2) convert basic to virtualbasic, 3) compact basic, 4) merge all insert files and main virtualbasic code into one file
	if choice[0] == "1":
		arguments = []
		try:
			if choice[1]: arguments = choice[1:] # only in this case we have parameters
		except:
			pass

		o = Basic(lines=lines, arguments=arguments, sourceFileName=fileName[:-4]) # arguments and sourceFileName are optionals
		o.root = path.dirname(fileName) # fix location to find insert files only if exists
		code = o.getAppleSoftBasic() # get the basic code
		if "e" in arguments or "-e" in arguments:
			code = "NEW\n"+code+"\nRUN\n" # ready to paste in emulator
		messages = o.msg # get warnings or success messages
		newFile = fileName[:-4] + ".bas"
		del o
	elif choice[0] == "2":
		o = BasicToVirtual(lines)
		code = o.getBasicToVirtualBasic()
		messages = o.msg # get warnings or success messages
		newFile = fileName[:-4] + "_vb.baz"
		del o
	elif choice[0] == "3":
		o = CompactCode(lines)
		code = o.getCompactCode()
		messages = o.msg # get warnings or success messages
		newFile = fileName[:-4] + "_cp.bas"
		del o
	elif choice[0] == "4":
		o = MergeCode(lines)
		o.root = path.dirname(fileName) # fix location to find insert files if exist
		code = o.GetMergedAllInserts()
		messages = o.msg # get warnings or success messages
		newFile = fileName[:-4] + "_mg.baz"
		del o
	else:
		exit()
	
	print messages
	
	# create the new file
	try:
		f = open(newFile,"w")
		f.write(code)
		f.close()
		print "File created: ",newFile
	except:
		print "Cannot create new file",newFile
	
	exit()
