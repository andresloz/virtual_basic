﻿#!/usr/bin/env python
#*- coding: utf-8 -*-

import re
from time import gmtime, strftime
from os import path, getcwd

NAME = "VirtualBasic"
VERSION = "0.3.5 2011-2016"
AUTHOR = "Andres Lozano a.k.a Loz"
COPYRIGHT = "Copyleft: This is a free work, you can copy, distribute, and modify it under the terms of the Free Art License http://artlibre.org/licence/lal/en/"
URL = "http://virtualbasic.org"

class VirtualBasicCode:
	"""class VirtualBasicCode 0.3.5 2011-2016"""
	def __init__(self, lines=[], arguments=[], sourceFileName="none", step=10):
		self.lines = lines
		self.arguments = arguments
		self.step = step
		self.incr = step
		self.gotos = {}
		self.appels = {}
		self.reservedNumberedLines = []
		self.code = ""
		self.otherInsert = 0
		self.root = getcwd() # get directory from this script()
		self.msg = ""
		# self.sourceFileName = sourceFileName[len(sourceFileName)-200:]
		self.sourceFileName = path.basename(sourceFileName)
		
	def cleanLines(self):
		result = []
		s= ""
		crln = re.compile("\r|\n")
		for line in self.lines:
			s = crln.sub("",line)
			result.append(s)
		return result
		
	def deleteLinesWithHashes(self):
		"delete lines beginning with hash char"
		result = []
		lineWithHash = re.compile("^\s*\#")
		for line in self.lines:
			if not lineWithHash.search(line):
				result.append(line)
		self.lines = result
		
	def deleleteSlashes(self):
		"replace char with slashes"
		result = []
		for line in self.lines:
			line = line.replace("\\","")
			result.append(line)
		self.lines = result
		
	def deleteLinesWithRem(self):
		"delete lines beginning with rem"
		result = []
		lineWithHash = re.compile("^\s*rem", re.I)
		for line in self.lines:
			if not lineWithHash.search(line):
				result.append(line)
		self.lines = result

	def replaceLastHashes(self):
		"replace \# by :rem * unless it's pr# or with slashes"
		result = []
		hashes = re.compile("[^pr\\\]\#.*", re.I)
		for line in self.lines:
			line = hashes.sub("",line)
			result.append(line)
		self.lines = result
		
	def ignoreBracketedComments(self):
		"ignore bracketed comments"
		result = []
		# ignore lines between [...]
		flag = 0
		beginBracketedComment = re.compile("^\s*\[")
		endBracketedComment = re.compile("^\s*\]")
		for line in self.lines:
			# regex if first line with [ multiline comment begin
			if (beginBracketedComment.search(line) or flag == 1): 
				flag = 1		
			if flag == 0:
				result.append(line);
			if endBracketedComment.search(line): # regex if last line with ] multiline comment end
				flag = 0
		self.lines = result

	def insertFiles(self):
		"insert external files"
		self.otherInsert = 0
		result = []
		# insertFile = re.compile("===insert ([a-zA-Z0-9\/\-\_]+\.baz)===", re.I)
		insertFile = re.compile("===([a-zA-Z0-9\/\-\_\s]+\.baz)===", re.I)
		lineWithHash = re.compile("^\s*\#")
		for line in self.lines:
			if insertFile.search(line):
				m = insertFile.search(line)
				fileName = m.group(1)
				print fileName
				if fileName[0:7] == "insert " or fileName[0:7] == "insert ".upper():
					fileName = fileName[7:]
				fichier = self.root + "/" + fileName
				try:
					f = open(fichier,"r")
					fic = f.readlines()
					for linefic in fic:
						if not lineWithHash.search(linefic):
							result.append(linefic)
					f.close()
				except:
					self.msg += "\n! Warning can't insert file  " + fichier + " ! "
			else:
				result.append(line)
				
		insertFile = re.compile("===[a-zA-Z0-9\/\-\_\s]+\.baz===", re.I)
		for line in result:
			if insertFile.search(line):
				self.otherInsert = 1
				break
		
		self.lines = result
	
	def deleteInserts(self):
		"delete inserts"
		result = []
		insertFile = re.compile("===([a-zA-Z0-9\/\-\_\s]+\.baz)===", re.I)
		for line in self.lines:
			if insertFile.search(line): # if find insert then insert not done
				m = insertFile.search(line)
				fichier = self.root + "/" + m.group(1)
				self.msg += "\n! Warning can't insert file  " + fichier + ", deleted ! "
			line = insertFile.sub("",line)
			result.append(line)
		self.lines = result
		
	def ignoreEmptyLinesAndSection(self):
		"ignore empty lines and sections"
		result = []
		emptyLine = re.compile("^\s*$")
		beginSection = re.compile("^[\s]*section", re.I)
		endSection = re.compile("^[\s]*finsection", re.I)
		closeSection = re.compile("^[\s]*closesection", re.I)
		tabOrSpace = re.compile("^[\t\s]*")
		for line in self.lines:
			if not ( 
			emptyLine.search(line) 
			or beginSection.search(line) 
			or endSection.search(line) 
			or closeSection.search(line) 
			):
				line = tabOrSpace.sub("",line) # take line and erase blank lines
				result.append(line)
		self.lines = result

	def ignoreRemLinesWithSparks(self):
		"ignore lines beginning by REM * (spark)"
		result = []
		remLinesWithSpark = re.compile("^[\s]*rem \*", re.I)
		for line in self.lines:
			if not remLinesWithSpark.search(line):
				result.append(line)
		self.lines = result

	def reservesNumbers(self):
		"reserves lines with numbers"
		result = []
		numberedLines = re.compile("^([0-9]+)")
		for line in self.lines:
			if numberedLines.search(line): # protect line numbers
				m = numberedLines.search(line)
				result.append(m.group(1))
		self.reservedNumberedLines = result

	def numberUppercase(self):
		"number and upcase"
		result = []
		numberedLines = re.compile("^[0-9]+")
		for line in self.lines:
			if numberedLines.search(line): # if first line number
				result.append(line)
			else:
				self.step = self.step + self.incr
				if str(self.step) in self.reservedNumberedLines:
					self.step = self.step + self.incr 
				if line != "":
					result.append(str(self.step) + " " + line)
		for i in xrange( len( result ) ):
			result[i] = result[i].upper()
		self.lines = result

	def replaceGotoGosub(self):
		"find gotos and gosub"
		result = []
		gotosub = re.compile("^([0-9]+) "+"£"+"([A-Z]+)")
		for line in self.lines:
			if gotosub.search(line): # position of subscript
				m = gotosub.search(line)
				gotosubName = (int(m.group(1)) + self.incr) # add incr and go to first line
				self.gotos[m.group(2)] = str(gotosubName) # $1 = number of line, $2 = subscript name
				tmp1 = "£" + m.group(2)
				tmp2 = "REM->" + m.group(2)
				# put comment about subscript
				if not "u" in self.arguments or "-u" in self.arguments:
					line = re.sub(tmp1,tmp2,line)
					result.append(line)
			else:
				result.append(line)
		self.lines = result		

	def replaceCalls(self):
		"find and replace goto, gosub"
		result = []
		tmp = ""
		# [^\\\] not when char @ is slashed
		gotosubCall = re.compile("[^\\\]"+"@"+"[A-Z]+")
		gotosubCallAll = re.compile("[^\\\]"+"@"+"([A-Z]+)?")
		# gotosubCall = re.compile("[^\\\]"+"@"+"[a-zA-Z]+")
		# gotosubCallAll = re.compile("[^\\\]"+"@"+"([a-zA-Z]+)?")
		for line in self.lines:
			if gotosubCall.search(line): # find a call to subscript
				occurrences = gotosubCallAll.findall(line)
				for res in occurrences:
					tmp1 = "@" + res
					if self.gotos.has_key(res):
						tmp2 = self.gotos[res]
						self.appels[res] = "exist"
						tmp += res + " " # space to split multiple "go"
						# replace call by liee number of subscript
						line = re.sub(tmp1,tmp2,line)
					else:
						tmp = "???"	
						self.msg += "\n! Warning code not found for goto or gosub "
						self.msg += "\"" + res + "\" ! "	
				line = line.rstrip()
				if "remgo" in self.arguments or "-remgo" in self.arguments:
					line +=  ":REM GO->" + tmp 
				result.append(line)
				tmp = "";
			else:
				result.append(line)
		self.lines = result

	def linesSorted(self):
		"sort lines with numbers"
		result = []
		linesCode = {};myKey = ""
		linesNumbers = re.compile("^([0-9]+) (.*)$")
		for line in self.lines:
			if linesNumbers.search(line):
				m = linesNumbers.search(line)
				myKey = eval(m.group(1))
				linesCode[myKey] = m.group(2)
		KeysSorted = linesCode.keys()
		KeysSorted.sort()
		for myKey in KeysSorted:
			lineNumber = str(myKey)
			result.append(lineNumber + " " + linesCode[myKey])
		self.lines = result

	def deleteParenthesisComments(self):
		"delete parenthesis comments"
		result = []
		parenthesisComment = re.compile("\{.+?\}")
		for line in self.lines:
			line = parenthesisComment.sub("",line)
			result.append(line)
		self.lines = result

	def replacePrints(self):
		"replace print by ?"
		result = []
		prints = re.compile("PRINT")
		for line in self.lines:
			line = prints.sub("?",line)
			result.append(line)
		self.lines = result	

	def replaceDuplicatedColon(self):
		"replace :: by :"
		result = []
		duplicatedColon = re.compile("::")
		for line in self.lines:
			line = duplicatedColon.sub(":",line)
			result.append(line)
		self.lines = result

	def deleteRemLines(self,exceptLines=[]):
		"delete lines beginning by rem"
		result = []
		remLine = re.compile("^[0-9]+ REM")
		# remLine = re.compile("^[0-9]+ rem", re.I)
		numberLine = re.compile("^[0-9]+")
		for line in self.lines:
			if not remLine.search(line) or numberLine.search(line) in exceptLines:
				result.append(line)
		self.lines = result

	def deleteSpaces(self):
		"delete all useless spaces"
		result = []
		flag = 0
		for line in self.lines:
			tmp = ""
			for char in line:
				if char == "\"":
					if flag == 1:
						flag = 0
					else:
						flag = 1
				if flag == 0 and char != " ":
					tmp += char
				if flag == 1:
					tmp += char
			result.append(tmp)
		self.lines = result
		
	def deleteAllComments(self):
		"delete all comments"
		self.deleteLinesWithHashes()
		self.replaceLastHashes()
		self.deleteParenthesisComments() 
		self.ignoreBracketedComments()
		self.ignoreEmptyLinesAndSection()
		if ("u" in self.arguments or "-u" in self.arguments or "c" in self.arguments or "-c" in self.arguments):
			self.ignoreRemLinesWithSparks()
	
	def glueCode(self):
		"stacking lines with semicolon"
		tmp = ""
		result = []
		forbiddenWord = re.compile("for|goto|gosub|then|rem|return|"+"£"+"|"+"@", re.I)
		semicolonBegin = re.compile("^\:")
		for line in self.lines:
			if not forbiddenWord.search(line):
				lineWithoutReturn = line.strip()
				if len(tmp+":"+lineWithoutReturn) < 255:
					tmp += ":"+lineWithoutReturn
				else:
					tmp = semicolonBegin.sub("",tmp)
					result.append(tmp)
					tmp = ""
			else:
				if len(tmp) > 0:
					tmp = semicolonBegin.sub("",tmp)
					result.append(tmp)
					tmp = ""
				
				result.append(line)
		
		# check if last line is in tmp and tmp not added
		if len(tmp) > 0:
			tmp = semicolonBegin.sub("",tmp)
			result.append(tmp)
			tmp = ""
			
		self.lines = result
		
	def convert(self):
		"convert virtual basic into basic"
		self.cleanLines()
		self.deleteAllComments()
		
		if self.root != "":# if root not defined don't try to import insert
			self.insertFiles()
			self.deleteAllComments()
			while self.otherInsert == 1:# scan again lines to find a new insert
				self.deleteAllComments()
				self.insertFiles()
		else:
			self.deleteInserts()
			
		# first rem clean
		if "u" in self.arguments or "-u" in self.arguments:
			self.deleteLinesWithRem()
			
		self.deleteAllComments()		
		self.reservesNumbers()
		if "u" in self.arguments or "-u" in self.arguments:
			self.glueCode()
			
		self.numberUppercase()
		self.replaceGotoGosub()
		self.replaceCalls()
		self.linesSorted()

		if ("u" in self.arguments or "-u" in self.arguments or "c" in self.arguments or "-c" in self.arguments):
			self.replacePrints()
			
		self.replaceDuplicatedColon()

		if "u" in self.arguments or "-u" in self.arguments:
			self.deleteRemLines()

		versionDate = strftime("%d/%m/%Y - %Hh%M", gmtime())
		fileSource = ""
		if self.sourceFileName != "none": fileSource = self.sourceFileName.upper() + ".BAS - "
		tmp = str(self.incr)+" REM " +fileSource+versionDate
		if "loz" in self.arguments:
			tmp += " - BY - ANDRES - AKA - LOZ - COPYLEFT"
		self.lines.insert(0,tmp)

		if "u" in self.arguments or "-u" in self.arguments:
			self.deleteSpaces()
		
		self.deleleteSlashes()
		
class Basic(VirtualBasicCode):
	def __init__(self, lines=[], arguments=[], sourceFileName="none", step=10):
		VirtualBasicCode.__init__(self, lines=lines, arguments=arguments, sourceFileName=sourceFileName, step=step)

	def basic(self):
		"convert into applesoft basic"
		result = ""
		self.convert()
		self.msg += "\nCode converted to Applesoft Basic"
		result += "\n".join(self.lines)
		self.code = result
		return result
	
	def getAppleSoftBasic(self):
		return self.basic()
		
class Fusion(VirtualBasicCode):
	def __init__(self, lines=[], root=""):
		VirtualBasicCode.__init__(self, lines=lines)
		self.root = root
		
	def insertFiles(self):
		"insert external files"
		self.otherInsert = 0
		result = []
		insertFile = re.compile("===insert ([a-zA-Z0-9\/\-\_]+\.baz)===", re.I)
		lineWithHash = re.compile("^\s*\#")
		insertFileWithHash = re.compile("\#[\t\s]*===insert [a-zA-Z0-9\/\-\_]+\.baz===.*", re.I)
		
		for line in self.lines:
			if insertFile.search(line) and not insertFileWithHash.search(line):
				m = insertFile.search(line)
				fichier = self.root + "/" + m.group(1)
				try:
					f = open(fichier,"r")
					fic = f.readlines()
					for linefic in fic:
						if not lineWithHash.search(linefic):
							result.append(linefic.rstrip())
					f.close()
				except:
					self.msg += "\n! Warning can't insert file  " + fichier + " ! "
			else:
				result.append(line)
				
		for line in result:
			if insertFile.search(line) and not insertFileWithHash.search(line):
				self.otherInsert = 1
				break
		
		self.lines = result
		
	def deleteInserts(self):
		"delete inserts strings"
		result = []
		insertFile = re.compile("===insert ([a-zA-Z0-9\/\-\_]+\.baz)===", re.I)
		for line in self.lines:
			if insertFile.search(line):
				m = insertFile.search(line)
				fichier = self.root + "/" + m.group(1)
				self.msg += "\n! Warning can't insert file  " + fichier + ", insert  deleted ! "
			line = insertFile.sub("",line)
			result.append(line)
		self.lines = result
		
	def fusion(self):
		"merge all insert files in one file"
		self.cleanLines()
		if self.root != "":# if root not defined don't try to import insert
			self.insertFiles()
			while self.otherInsert == 1:# scan again lines to find a new insert
				self.insertFiles()
		self.deleteInserts()
		self.msg += "\nCode Merged"
		result = "\n".join(self.lines)
		return result
		
	def GetMergedAllInserts(self):
		return self.fusion()

class MergeCode(Fusion):
	def __init__(self, lines=[], root=""):
		Fusion.__init__(self, lines=lines, root=root)
		
class BasToBaz:
	"""class BasToBaz 0.1.0 2011-2015"""
	def __init__(self, lines=[]):
		self.lines = lines
		self.links = {}
		self.lastRemName = ""
		self.msg = ""
		self.ismes = ["absolutism","absurdism","academicism","accidentalism","acosmism","adamitism","adevism","adiaphorism","adoptionism","aestheticism","agapism","agathism","agnosticism","anarchism","animism","annihilationism","anthropomorphism","anthropotheism","antidisestablishmentarianism","antilapsarianism","antinomianism","antipedobaptism","apocalypticism","asceticism","aspheterism","atheism","atomism","autosoterism","autotheism","bitheism","bonism","bullionism","capitalism","casualism","catabaptism","catastrophism","collectivism","collegialism","conceptualism","conservatism","constructivism","cosmism","cosmotheism","deism","determinism","diphysitism","ditheism","ditheletism","dualism","egalitarianism","egoism","egotheism","eidolism","emotivism","empiricism","entryism","epiphenomenalism","eternalism","eudaemonism","euhemerism","existentialism","experientialism","fallibilism","fatalism","fideism","finalism","fortuitism","functionalism","geocentrism","gnosticism","gradualism","gymnobiblism","hedonism","henism","henotheism","historicism","holism","holobaptism","humanism","humanitarianism","hylicism","hylomorphism","hylopathism","hylotheism","hylozoism","idealism","identism","ignorantism","illuminism","illusionism","imagism","immanentism","immaterialism","immoralism","indifferentism","individualism","instrumentalism","intellectualism","interactionism","introspectionism","intuitionism","irreligionism","kathenotheism","kenotism","laicism","latitudinarianism","laxism","legalism","liberalism","libertarianism","malism","materialism","mechanism","meliorism","mentalism","messianism","millenarianism","modalism","monadism","monergism","monism","monophysitism","monopsychism","monotheism","monotheletism","mortalism","mutualism","nativism","naturalism","necessarianism","neonomianism","neovitalism","nihilism","nominalism","nomism","noumenalism","nullibilism","numenism","objectivism","omnism","optimism","organicism","paedobaptism","panaesthetism","pancosmism","panegoism","panentheism","panpsychism","pansexualism","panspermatism","pantheism","panzoism","parallelism","pejorism","perfectibilism","perfectionism","personalism","pessimism","phenomenalism","physicalism","physitheism","pluralism","polytheism","positivism","pragmatism","predestinarianism","prescriptivism","primitivism","privatism","probabiliorism","probabilism","psilanthropism","psychism","psychomorphism","psychopannychism","psychotheism","pyrrhonism","quietism","racism","rationalism","realism","reductionism","regalism","representationalism","republicanism","resistentialism","romanticism","sacerdotalism","sacramentarianism","scientism","self-determinism","sensationalism","siderism","skepticism","socialism","solarism","solifidianism","solipsism","somatism","spatialism","spiritualism","stercoranism","stoicism","subjectivism","substantialism","syndicalism","synergism","terminism","thanatism","theism","theocentrism","theopantism","theopsychism","thnetopsychism","titanism","tolerationism","totemism","transcendentalism","transmigrationism","trialism","tritheism","triumphalism","tuism","tutiorism","tychism","ubiquitarianism","undulationism","universalism","utilitarianism","vitalism","voluntarism","zoism","zoomorphism","zootheism"]
		self.ismes.reverse()
		
	def numberToWord(self,number):
		nums = {"0":"Zero", "1":"One", "2":"Two", "3":"Three", "4":"Four", 
		"5":"Five", "6":"Six", "7":"Seven", "8":"Eight", "9":"Nine"}
		x = str(number)
		s = ""
		for e in x:
			s += nums[e]
		return s[0].lower() + s[1:]
		
	def ismeName(self,number):
		ismeVal = self.ismes.pop()
		return ismeVal
		
	def findLinks(self,lines):
		result = []
		liste = []
		gotoRe = re.compile("goto([ 0-9]+)")
		gosubRe = re.compile("gosub([ 0-9]+)")
		thenRe = re.compile("then([ 0-9]+)")
		# gotoRe = re.compile("goto([ 0-9]+)", re.I)
		# gosubRe = re.compile("gosub([ 0-9]+)", re.I)
		# thenRe = re.compile("then([ 0-9]+)", re.I)
		for line in lines:
			if gotoRe.search(line):
				occurrences = gotoRe.findall(line)
				for res in occurrences:
					if res.strip() not in result:
						result.append(res.strip())
			if gosubRe.search(line):
				occurrences = gosubRe.findall(line)
				for res in occurrences:
					if res.strip() not in result:
						result.append(res.strip())
			if thenRe.search(line):
				occurrences = thenRe.findall(line)
				for res in occurrences:
					if res.strip() not in result:
						result.append(res.strip())		
		return filter(None,result)		

	def lowerText(self):
		result = []
		s = ""
		for line in self.lines:
			s = line.lower()
			result.append(s)
		return result

	def cleanLines(self):
		result = []
		s= ""
		crln = re.compile("\r|\n")
		for line in self.lines:
			s = crln.sub("",line)
			result.append(s)
		return result
		
	def deleteFirstSpace(self):
		result = []
		s = ""
		space = re.compile("^ +")
		for line in self.lines:
			s = space.sub("",line)
			result.append(s)
		return result

	def createLinksMap(self):
		result = {}
		liste = self.findLinks(self.lines)
		listeInt = map(int,liste)
		listeInt.sort()
		for e in listeInt:
			result[str(e)] = self.ismeName(e)
			# result[str(e)] = self.numberToWord(e)
		return result

	def placeLinks(self):
		result = []
		s = ""
		numberFirst = re.compile("^([0-9]+)")
		# special for converted virtual basic scripts
		byName = re.compile("^([0-9 ]+)rem ->([a-zA-Z]+)")
		# byName = re.compile("^([0-9 ]+)rem ->([a-zA-Z]+)", re.I)
		remName = re.compile("rem->([a-z]+)")
		for line in self.lines:
			if numberFirst.search(line):  # numbered lines
				m = numberFirst.search(line)
				if remName.search(line): # if rem->link
					m3 = remName.search(line)
					self.lastRemName = m3.group(1)
					
				if self.links.has_key(m.group(1)): # if key in links list
					if  byName.search(line):
						m2 = byName.search(line)
						s = "£" + m2.group(2)
						self.links[m.group(1)] = m2.group(2)
						self.lastRemName = ""
					elif self.lastRemName != "":
						s = numberFirst.sub("",line) 
						s = "£" + self.lastRemName + "\n" + s.strip()
						self.links[m.group(1)] = self.lastRemName
						self.lastRemName = ""
						del result[-1] # remove last element of result because is lastRemName
					else:
						s = numberFirst.sub("",line) 
						s = "£" + self.links[m.group(1)] + "\n" + s.strip()
						self.lastRemName = ""
				else:  # not in links list
					s = line
			else: # not numbered lines
				s = line
			result.append(s)
		return result
		
	def placeCalls(self):
		result = []
		s = ""
		gotoRe = re.compile("goto([ 0-9]+)", re.I)
		gosubRe = re.compile("gosub([ 0-9]+)", re.I)
		thenRe = re.compile("then([ 0-9]+)", re.I)
		for line in self.lines:
			if gotoRe.search(line):
				occurrences = gotoRe.findall(line)
				for res in occurrences:
					x = res.strip()
					if x != "":
						# res = re.compile("goto"+res, re.I)
						# line = res.sub("goto @" + self.links[x], line)
						line = line.replace("goto"+res,"goto @" + self.links[x])
			if gosubRe.search(line):
				occurrences = gosubRe.findall(line)
				for res in occurrences:
					x = res.strip()
					if x != "":
						# res = re.compile("gosub"+res, re.I)
						# line = res.sub("gosub @" + self.links[x], line)
						line = line.replace("gosub"+res,"gosub @" + self.links[x])
			if thenRe.search(line):
				occurrences = thenRe.findall(line)
				for res in occurrences:
					x = res.strip()
					if x != "":
						# res = re.compile("then"+res, re.I)
						# line = res.sub("then @" + self.links[x], line)
						line = line.replace("then"+res,"then @" + self.links[x])	
			result.append(line)
		return result
		
	def placeCallsOn(self):
		result = []
		s = ""
		onRe = re.compile(",([ 0-9]+)")
		for line in self.lines:
			if onRe.search(line):
				occurrences = onRe.findall(line)
				for res in occurrences:
					x = res.strip()
					if x != "" and self.links.has_key(x):
						line = line.replace(res,"@" + self.links[x])
			result.append(line)
		return result

	def delLinesNumbers(self):
		result = []	
		s = ""
		lineNumber = re.compile("^([ 0-9]+)")
		for line in self.lines:
			s = lineNumber.sub("",line)
			result.append(s)
		return result
	
	def toVirtual(self):
		self.lines = self.cleanLines()
		self.lines = self.deleteFirstSpace()
		self.lines = self.lowerText()
		self.links = self.createLinksMap()
		self.lines = self.placeLinks()
		self.lines = self.placeCalls()
		self.lines = self.placeCallsOn()
		self.lines = self.delLinesNumbers()
		self.msg += "\nCode converted to VirtualBasic"
		return '\n'.join(self.lines)
		
	def getBasicToVirtualBasic(self):
		return self.toVirtual()

class BasicToVirtual(BasToBaz):
	def __init__(self, lines=[], root=""):
		BasToBaz.__init__(self, lines=lines)
		
class CompactCode:
	"""class CompactCode 0.1.0 2011-2015"""
	def __init__(self, lines=[], step=10):
		# first step convert to virtualbasic
		obj = BasToBaz(lines)
		basicLines = obj.getBasicToVirtualBasic()
		self.msg = obj.msg
		del obj
		# second step convert to basic
		basicLines = basicLines.split("\n") # convert text to list of lines
		obj = Basic(lines=basicLines, arguments=['u'], step=step)
		self.lines = obj.getAppleSoftBasic()
		self.msg += obj.msg
		self.msg += "\nCode compacted"
		del obj
		
	def getCompactCode(self):
		return self.lines

class Compression(CompactCode):
	def __init__(self, lines=[]):
		CompactCode.__init__(self, lines=lines)
		
	def compression(self):
		"compact code"
		self.getCompactCode()
		
if __name__ == "__main__":
	print NAME
	print VirtualBasicCode.__doc__
	print "\n".join([AUTHOR,COPYRIGHT,URL])
