#!/usr/bin/env python
#*- coding: utf-8 -*-

from os import path, getcwd, name as thisSystem
import codecs
import re
from sys import getsizeof

from Tkinter import *
import tkFileDialog
import tkMessageBox
import tkSimpleDialog
import tkFont

from subprocess import Popen

from virtualbasic import VirtualBasicCode, Basic, BasicToVirtual, MergeCode, CompactCode

menuAddons = [] 
# ansi_to_utf addons to main menu
# try: 
	# from u_convUtf import convertFiles2utf8 # script to convert olds virtualbasic files coded in latin-1
	# menuAddons.append('\t\tmAddons.add_command(label="Convert file to utf-8", command=lambda: convertFiles2utf8(mod="file"))\n')
	# menuAddons.append('\t\tmAddons.add_command(label="Convert Folder files to utf-8", command=lambda: convertFiles2utf8(mod="folder"))\n')
# except:
	# pass

appleWin = ""
if thisSystem == "nt" and path.isfile("C:/AppleWin/Applewin.exe"): # windows system
	appleWin = "C:/AppleWin/Applewin.exe"
	
NAME = "Graphical User Interface for VirtualBasic"
VERSION = "0.0.9 2016"
AUTHOR = "Andres Lozano a.k.a Loz"
COPYRIGHT = "Copyleft: This is a free work, you can copy, distribute, and modify it under the terms of the Free Art License http://artlibre.org/licence/lal/en/"
URL = "http://virtualbasic.org"

class virtualBasicEditor():
	"""Class virtualBasicEditor Graphical user interface for virtualBasic 0.0.9"""
	def __init__(self):
		# virtualbasic class vars
		self.root = getcwd()
		self.virtualOpenRoot = getcwd()
		self.basicOpenRoot = getcwd()
		self.virtualSaveRoot  = getcwd()
		self.basicSaveRoot = getcwd()
		self.sourceFileName = "none"
		# field vars
		self.fileVirtualOpened = ""
		self.fieldVirtualBackup = ""
		self.fileBasicOpened = ""
		self.fieldBasicBackup = ""
		self.buffers = {}
		self.bufferFileNames = []
		self.undoField = "virtualBasic"
		# stats vars
		self.statsLinesVirtual = 0
		self.statsCharsVirtual = 0
		self.statsLinesBasic = 0
		self.statsCharsBasic = 0
		# window vars and draw interface
		self.tkRoot = Tk()
		self.tkRoot.title("VirtualBasic Editor")
		self.fontSize = 11
		self.fontFamily = "Arial"
		self.customFont = tkFont.Font(family=self.fontFamily, size=self.fontSize)
		self.customFontBold = tkFont.Font(family=self.fontFamily, size=self.fontSize, weight="bold")
		# draw interface
		self.drawInterface()
		
		# init find and replace
		self.stepIncr = IntVar()
		self.stepIncr.set(10)
		self.searchText = StringVar()
		self.replaceText = StringVar()
		self.nextFind = 1.0
		self.selectedInField = ""
		self.fieldFocus = "virtualBasic"
		self.winFindPos = ""
		self.regexStatus = IntVar()
		self.regexStatus.set(0)
		self.caseSensitiveStatus = IntVar()
		self.caseSensitiveStatus.set(0) 
		self.searchInBasicStatus = IntVar()
		self.searchInBasicStatus.set(0)
		
		self.tkRoot.mainloop()
		
	def quitInterface(self, event=None):
		self.tkRoot.destroy()
		
	def focusInFieldVirtual(self, event=None):
		self.fieldFocus = "virtualBasic"
		
	def focusInFieldBasic(self, event=None):
		self.fieldFocus = "appleSoftBasic"
		
	def changeVirtualRootProject(self, event=None):# change the root dir of the project to keep ok the path of the inserts/imports
		dirName = tkFileDialog.askdirectory(parent=self.tkRoot, title="Choose root directory") 
		if dirName:
			self.root = dirName
			self.labelRootVirtual.config(text="Root project "+self.root)
		
		return "break" # prevent for bind->handler in Text class
		
	def selectallVirtual(self, event=None):
		self.fieldVirtual.tag_add(SEL, "1.0", END)
		self.fieldVirtual.focus_set()# for manual copying
		self.status("select all virtualBasic")
		return "break"
		
	def selectallBasic(self, event=None):
		self.fieldBasic.tag_add(SEL, "1.0", END)
		self.fieldBasic.focus_set() # for manual copying
		self.status("select all AppleSoft Basic")
		return "break"
		
	def copyBasic(self, event=None):
		self.fieldBasic.tag_add(SEL, "1.0", END)
		self.fieldBasic.focus_set() # for manual copying
		self.tkRoot.clipboard_clear()
		self.tkRoot.clipboard_append(self.fieldBasic.get(0.1, END).encode('utf-8'))
		self.status("AppleSoft Basic copied to clipboard")
		return "break" # prevent for bind->handler in Text class
	
	def splitDbPointBasic(self):
		content = self.fieldBasic.get(0.1, END).encode('utf-8')
		dbPoint = re.compile("\s?:\s?")
		# content = content.replace(":","\n")
		content = dbPoint.sub("\n",content)
		self.fieldBasic.delete(0.1, END)
		self.fieldBasic.insert(INSERT, content)
		return "break"
	
	def addFileToBuffers(self, field="", file="", content=""):
		if file not in self.buffers.keys():# avoid duplication
			self.mBuffers.add_command(label=file, command=lambda: self.showFileContent(field=field, file=file))
		
		self.buffers.update({(file, content)})
		self.bufferFileNames.append(file)
		
	def deleteBufferEntry(self, event=None):
		if len(self.bufferFileNames):# while items
			fileName = self.bufferFileNames.pop()# get last entry
			self.mBuffers.delete(fileName)# delete item menu
			del self.buffers[fileName]# delete dict entry
			if self.fileVirtualOpened == fileName:# if buffer in use
				self.updateFieldInfo(field="virtualBasic")# reset field
			if self.fileBasicOpened == fileName:
				self.updateFieldInfo(field="appleSoftBasic")
			
	def showFileContent(self, field="", file=""):
		self.backupField()# empty backup avoid field content confusion
		if field == "virtualBasic" and self.fileVirtualOpened != file: # only if file back is different
			content = self.fieldVirtual.get(0.1, END).encode('utf-8')
			self.buffers.update({(self.fileVirtualOpened, content)})# save work in field
			self.backupField(field) # undo remenber
			self.fieldVirtual.delete(0.1, END)
			self.fieldVirtual.insert(INSERT, self.buffers[file])# get back the data
			self.fieldVirtual.highLightingText()	
			self.updateFieldInfo(field=field, file=file, size=str(getsizeof(self.buffers[file])))
			
		if field == "appleSoftBasic" and self.fileBasicOpened != file:
			content = self.fieldBasic.get(0.1, END).encode('utf-8')
			self.buffers.update({(self.fileBasicOpened, content)})
			self.backupField(field)
			self.fieldBasic.delete(0.1, END)
			self.fieldBasic.insert(INSERT, self.buffers[file])
			self.fieldBasic.highLightingText()
			self.updateFieldInfo(field=field, file=file, size=str(getsizeof(self.buffers[file])))
			
	def updateFieldInfo(self, field="?", file="?", size="?", msg="file"):# update info about file in label file, field, label dir and root vars
		if field == "virtualBasic" and file != "?":
			self.fieldFocus = field
			self.sourceFileName = file[:-4]
			self.root = path.dirname(file)
			self.labelRootVirtual.config(text="Root project "+self.root)
			self.fileVirtualOpened = file
			self.labelFileVirtual.config(text="file "+self.fileVirtualOpened)
			self.fieldVirtual.highLightingText()
		elif field == "appleSoftBasic" and file != "?":
			self.fieldFocus = field
			self.fileBasicOpened = file
			self.labelFileBasic.config(text="file "+self.fileBasicOpened)
			self.fieldBasic.highLightingText()
		elif field == "virtualBasic":
			self.fieldFocus = field
			self.fieldVirtual.delete(0.1, END)
			self.sourceFileName = ""
			self.root = ""
			self.fileVirtualOpened = ""
			self.labelFileVirtual.config(text="file "+self.fileVirtualOpened)
			self.labelRootVirtual.config(text="Root project "+self.root)
		elif field == "appleSoftBasic":
			self.fieldFocus = field
			self.fileBasicOpened = ""
			self.fieldBasic.delete(0.1, END)
			self.labelFileBasic.config(text="file "+self.fileBasicOpened)
		else:
			# nothing 
			return 0
		# status 
		self.status(" ".join([msg,field,file,"size:",size,"bytes"]))
		return 1
	
	def fieldVirtualEnlarge(self, event=None):
		sizeVirtual  = self.fieldVirtual.winfo_width()-50
		self.tkRoot.grid_columnconfigure(0, weight=1, minsize=sizeVirtual)
		return "break" # prevent for bind->handler in Text class
		
	def fieldVirtualShrink(self, event=None):
		sizeVirtual  = self.fieldVirtual.winfo_width()+50
		self.tkRoot.grid_columnconfigure(0, minsize=sizeVirtual)
		return "break" # prevent for bind->handler in Text class
		
	def zoomTextIn(self, event=None):
		self.fontSize += 1
		self.customFont.configure(size=self.fontSize)
		self.customFontBold.configure(size=self.fontSize)
		self.status("font size "+str(self.fontSize))
		return "break" # prevent for bind->handler in Text class
	
	def zoomTextOut(self, event=None):
		self.fontSize -= 1
		self.customFont.configure(size=self.fontSize)
		self.customFontBold.configure(size=self.fontSize)
		self.status("font size "+str(self.fontSize))
		return "break" # prevent for bind->handler in Text class
		
	def about(self):
		msg = "\n".join([AUTHOR,URL])
		tkMessageBox.showinfo("Author", msg)
		
	def virtualBasicEditorInfos(self):
		msg = "\n".join([NAME, VERSION, VirtualBasicCode.__doc__, AUTHOR, URL, COPYRIGHT])
		tkMessageBox.showinfo("Virtual Basic Editor infos", msg)
		
	def statsOnCode(self):
		msg = "virtualBasic:"
		if self.fileVirtualOpened:msg += "\nfile "+self.fileVirtualOpened
		msg += "\nlines "
		msg += self.fieldVirtual.index("end-1c").split(".")[0]
		msg += "\nchars "
		msg += str(len(self.fieldVirtual.get(0.1, END)))
		msg += "\nbytes "
		msg += str(getsizeof(self.fieldVirtual.get(0.1, END)))
		msg += "\n\n"+"AppleSoft Basic"
		
		if self.fileBasicOpened:msg += "\nfile "+self.fileBasicOpened
		msg += "\nlines "
		msg += self.fieldBasic.index("end-1c").split(".")[0]
		msg += "\nchars "
		msg += str(len(self.fieldBasic.get(0.1, END)))
		msg += "\nbytes "
		msg += str(getsizeof(self.fieldBasic.get(0.1, END)))
		msg += "\n\nlast backup field "
		msg += self.undoField
		msg += "\nbuffers "
		msg += str(len(self.bufferFileNames)) 
		msg += "\nfont "+self.fontFamily+" "+str(self.fontSize)
		tkMessageBox.showinfo("Stats", msg)
		
	def askLineNumberStep(self, event=None):
		num = tkSimpleDialog.askinteger("Define line number step","Define line number step", initialvalue=self.stepIncr.get())
		if num: self.stepIncr.set(num)
		
	def undoLastEdit(self, event=None):
		if self.undoField == "virtualBasic":
			self.fieldVirtual.delete(0.1, END)
			self.fieldVirtual.insert(INSERT, self.fieldVirtualBackup)
			self.fieldVirtual.highLightingText()
			self.status("field virtualBasic restored")
		elif self.undoField == "appleSoftBasic":
			self.fieldBasic.delete(0.1, END)
			self.fieldBasic.insert(INSERT, self.fieldBasicBackup)
			self.fieldBasic.highLightingText()
			self.status("field AppleSoft Basic restored")
		else:
			self.status("Nothing to restore","red")
		
		return "break" # prevent for bind->handler in Text class
	
	def backupField(self, field=""):# save content of field and set undo to field
		self.undoField = field
		if field == "virtualBasic": 
			self.fieldVirtualBackup = self.fieldVirtual.get(0.1, END).encode('utf-8') # backup virtual
		elif field == "appleSoftBasic": 
			self.fieldBasicBackup = self.fieldBasic.get(0.1, END).encode('utf-8') # backup basic
		else:
			self.undoField = "" # no backup
			
	def virtualToBasic(self, event=None, arguments=[]):# convert virtualBasic to AppleSoft Basic
		content = self.fieldVirtual.get(0.1, END).encode('utf-8') # don't forget to encode field text to be compatible
		lines = content.split("\n")
		o = Basic(lines, arguments, sourceFileName=self.sourceFileName, step=self.stepIncr.get())# create a Basic object from virtualbasic.py
		if self.root: 
			self.labelRootVirtual.config(text="Root project "+self.root)
			o.root = self.root
			
		codeBasic = o.getAppleSoftBasic()
		print o.msg
		
		self.backupField("appleSoftBasic")
		self.fieldBasic.delete(0.1, END)
		self.fieldBasic.insert(INSERT, codeBasic)
		self.fieldBasic.highLightingText()
		
		# highlight virtual panel
		self.fieldVirtual.highLightingText()
		
		# copy to clipboard
		self.tkRoot.clipboard_clear()
		self.tkRoot.clipboard_append(codeBasic.encode('utf-8'))
		return str(getsizeof(codeBasic)), str(codeBasic.count('\n'))
		
	def virtualToBasicStd(self, event=None):
		size,lines = self.virtualToBasic()# call unique function virtualToBasic
		self.status("virtualBasic converted to AppleSoft Basic, "+lines+" lines, size: "+size+" bytes")
		return "break" # prevent for bind->handler in Text class
		
	def virtualToBasicLight(self, event=None):# convert virtualBasic to AppleSoft Basic light
		size,lines = self.virtualToBasic(arguments=["c"])
		self.status("virtualBasic converted to AppleSoft Basic (light), "+lines+" lines, size: "+size+" bytes")
		return "break" # prevent for bind->handler in Text class
		
	def virtualToBasicCompacted(self, event=None):# convert virtualBasic to AppleSoft Basic ultra compacted
		size,lines = self.virtualToBasic(arguments=["u"])
		self.status("virtualBasic converted to AppleSoft Basic (Ultra compact), "+lines+" lines, size: "+size+" bytes")
		return "break" # prevent for bind->handler in Text class
		
	def virtualToBasicRemgo(self, event=None):# with more comments
		size,lines = self.virtualToBasic(arguments=["remgo"])
		self.status("virtualBasic converted to AppleSoft Basic (more comments), "+lines+" lines, size: "+size+" bytes")
		
	def virtualToBasicLoz(self, event=None):# with author name, hidden shorcut Control-Alt-b
		size,lines = self.virtualToBasic(arguments=["c","loz"])
		self.fieldBasic.insert(1.0, "NEW\n")
		self.fieldBasic.insert(END, "\nRUN")
		# copy again to clipboard
		self.tkRoot.clipboard_clear()
		self.tkRoot.clipboard_append(self.fieldBasic.get(0.1, END).encode('utf-8'))
		self.fieldBasic.highLightingText()
		self.status("virtualBasic converted to AppleSoft Basic (loz), "+lines+" lines, size: "+size+" bytes")
		return "break" # prevent for bind->handler in Text class
		
	def virtualMerge(self):# merge in one file all inserts/imports files and code
		content = self.fieldVirtual.get(0.1, END).encode('utf-8')
		lines = content.split("\n")
		o = MergeCode(lines)
		if self.root: 
			o.root = self.root
			self.labelRootVirtual.config(text="Root project "+self.root)
			
		codeVirtual = o.GetMergedAllInserts()
		print o.msg
		self.backupField("virtualBasic")
		self.fieldVirtual.delete(0.1, END)
		self.fieldVirtual.insert(INSERT, codeVirtual)
		self.fieldVirtual.highLightingText()
		self.status("VirtualBasic merged, "+str(codeVirtual.count('\n'))+" lines, size: "+str(getsizeof(codeVirtual))+" bytes")
		
	def basicToVirtual(self):# convert Applesoft Basic to virtualBasic
		content = self.fieldBasic.get(0.1, END).encode('utf-8')
		lines = content.split("\n")
		o = BasicToVirtual(lines)
		codeVirtual = o.getBasicToVirtualBasic()
		print o.msg
		
		self.backupField("virtualBasic")
		self.fieldVirtual.delete(0.1, END)
		self.fieldVirtual.insert(INSERT, codeVirtual)
		self.fieldVirtual.highLightingText()
		self.status("Basic converted to virtualBasic, "+str(codeVirtual.count('\n'))+" lines, size: "+str(getsizeof(codeVirtual))+" bytes")
		
	def compactBasic(self):# try to compact AppleSoft Basic as better as possible
		content = self.fieldBasic.get(0.1, END).encode('utf-8')
		lines = content.split("\n")
		o = CompactCode(lines,step=self.stepIncr.get())
		codeBasic = o.getCompactCode()
		print o.msg
		
		self.backupField("appleSoftBasic")
		self.fieldBasic.delete(0.1, END)
		self.fieldBasic.insert(INSERT, codeBasic)
		self.status("AppleSoft Basic compacted, "+str(codeBasic.count('\n'))+" lines, size: "+str(getsizeof(codeBasic))+" bytes")
	
	def cleanLines(self, lines=[]):# rip all end lines
		result = []
		s= ""
		crln = re.compile("\r|\n")
		for line in lines:
			s = crln.sub("",line)
			result.append(s)
		return result
		
	def refreshHighLighting(self, event=None):
		self.fieldVirtual.highLightingText()
		self.fieldBasic.highLightingText()
		self.status("Syntax highlighting refresh")
		return "break" # prevent for bind->handler in Text class
		
	def status(self, text="", color=""):
		if not color:color="black"
		self.labelStatus.config(text=text, foreground=color)
	
	def getSelection(self, event):# get selected text
		if self.fieldFocus == "virtualBasic":
			field = self.fieldVirtual	
		else:
			field = self.fieldBasic
				
		try:# if no selection get tcl error message
			self.selectedInField = field.selection_get()
			self.nextFind = field.index("sel.last")
		except:# reset find
			self.selectedInField = ""
			self.nextFind = 1.0
			
	def findAndReplaceWin(self, event=None):
		try:
			self.winFindPos = self.findReplaceWin.geometry()
			self.findReplaceWin.destroy()
		except:
			pass
			
		# draw the find/replace dialog
		self.findReplaceWin = Toplevel(self.tkRoot)
		self.findReplaceWin.title("Find/Replace Ctrl+N next")
		self.findReplaceWin.protocol("WM_DELETE_WINDOW", self.quitFindReplaceWin)
		
		# top frame
		topFrame = Frame(self.findReplaceWin)
		
		# label find and entry word
		findWord = Label(topFrame, text="Find", font=self.customFont)
		self.eFind = Entry(topFrame, textvariable=self.searchText, width=40, font=self.customFont, relief=SUNKEN)
		if self.selectedInField: self.searchText.set(self.selectedInField) # put selected text into entry
		 
		# label replace and entry word
		replaceWord = Label(topFrame, text="Replace", font=self.customFont)
		self.eReplace = Entry(topFrame, textvariable=self.replaceText, width=40, font=self.customFont, relief=SUNKEN)
		
		# buttons
		buttonFindNext = Button(topFrame, text="Find Next", font=self.customFont, command=self.doFindText)
		buttonFindReplace = Button(topFrame, text="Replace", font=self.customFont, command=self.doReplaceText)
		buttonFindReplaceAll = Button(topFrame, text="Replace All", font=self.customFont, command=self.doReplaceTextAll)
		buttonCancel = Button(topFrame, text="Cancel", font=self.customFont, command=self.quitFindReplaceWin)

		# bottom frame
		bottomFrame = Frame(self.findReplaceWin)
		
		# regex 
		self.regexButton = Checkbutton(bottomFrame, text="RegExp", font=self.customFont, variable=self.regexStatus)
		if self.regexStatus.get(): self.regexButton.select()
		
		# case sensitive
		self.caseSensitiveButton = Checkbutton(bottomFrame, text="Case Sensitive", font=self.customFont, variable=self.caseSensitiveStatus)
		if self.caseSensitiveStatus.get(): self.caseSensitiveButton.select() # because value is 
		
		# search in
		self.searchInBasic = Checkbutton(bottomFrame, text="Field Basic", font=self.customFont, variable=self.searchInBasicStatus)
		if self.fieldFocus == "appleSoftBasic": 
			self.searchInBasic.select()
		else:
			self.searchInBasic.deselect()
		
		# status
		self.findReplaceStatus = Label(bottomFrame, text="status")
		
		# grid
		topFrameGrid = [
			[findWord, 						0, 0, 1, 1, W],
			[self.eFind, 					0, 1, 1, 1, E],
			[replaceWord, 					1, 0, 1, 1, W],
			[self.eReplace,					1, 1, 1, 1, E],
			[buttonFindNext, 				0, 2, 1, 1, W+E],
			[buttonFindReplace, 			1, 2, 1, 1, W+E],
			[buttonFindReplaceAll, 			2, 1, 1, 1, W],
			[buttonCancel, 					2, 2, 1, 1, W+E]
		]
		topFrame.grid(row=0, column=0)
		for w in topFrameGrid:
			w[0].grid(row=w[1], column=w[2], rowspan=w[3], columnspan=w[4], sticky=w[5])
		
		bottomFrameGrid = [
			[self.regexButton, 				0, 0, 1, 1, W],
			[self.caseSensitiveButton, 		0, 1, 1, 1, W],
			[self.searchInBasic, 			0, 2, 1, 1, W],
			[self.findReplaceStatus, 		1, 0, 1, 3, W]
		]
		bottomFrame.grid(row=1, column=0, sticky=E+W)
		for w in bottomFrameGrid:
			w[0].grid(row=w[1], column=w[2], rowspan=w[3], columnspan=w[4], sticky=w[5])
		
		# shorcuts
		self.findReplaceWin.bind("<Return>",self.doFindText)
		self.findReplaceWin.bind("<Control-n>",self.doFindText)
		
		# win pos, geometry
		self.findReplaceWin.withdraw() # hide win
		# self.findReplaceWin.update()
		if self.winFindPos: 
			self.findReplaceWin.geometry(self.winFindPos)
		else:
			self.winFindPos = self.findReplaceWin.geometry()
		
		self.findReplaceWin.deiconify()# show win
		self.findReplaceWin.mainloop()
		return "break" # prevent for bind->handler in Text class
		
	def quitFindReplaceWin(self, event=None):
		self.winFindPos = self.findReplaceWin.geometry()
		self.findReplaceWin.destroy()
		
	def doFindText(self, event=None):			
		if self.searchInBasicStatus.get(): # check choosed field to search
			field = self.fieldBasic
		else:
			field = self.fieldVirtual
			
		field.tag_remove("find", 1.0, END) # reset found text tag	
		# self.searchText = self.eFind.get() # get entry find
		lenSearch = StringVar()
		pos = field.search(self.searchText.get(), self.nextFind, stopindex="end", nocase=abs(self.caseSensitiveStatus.get()-1), regexp=self.regexStatus.get(), count=lenSearch)
		if not pos:
			self.findTextPos = None
			self.nextFind = 1.0
			self.findReplaceStatus.configure(text="not found or end of text", foreground="red")
			self.status(self.searchText.get()+" not found or end of text","red")
			return
	
		line, col = pos.split('.')
		self.findTextPos = (pos,"%s.%d"%(line, int(col)+int(lenSearch.get()))) # remenber find pos and length
		self.nextFind = pos + "+"+lenSearch.get()+"c"
		
		field.tag_add("find", pos, "%s.%d"%(line, int(col)+int(lenSearch.get())))
		field.see(pos) # need to scroll to found text
		
		self.findReplaceStatus.configure(text=self.searchText.get()+" found at ln:"+line+" ch:"+col, foreground="black")
		self.status(self.searchText.get()+" found at ln:"+line+" ch:"+col)
		
	def doReplaceText(self, event=None):
		if self.searchInBasicStatus.get():# choosed field to search
			field = self.fieldBasic
		else:
			field = self.fieldVirtual
	
		if self.findTextPos:# check if find pos is ok
			field.delete(self.findTextPos[0],self.findTextPos[1])# delete old text
			field.insert(self.findTextPos[0],self.replaceText.get())# insert replace text
			self.nextFind = self.findTextPos[0] + "+"+str(len(self.replaceText.get()))+"c" # update next find pos with new length
		else:
			self.findReplaceStatus.configure(text="no find to replace")
			
	def doReplaceTextAll(self, event=None):
		if self.searchInBasicStatus.get():# choosed field to search
			field = self.fieldBasic
		else:
			field = self.fieldVirtual
			
		replaceTimes = 0 # set counter
	
		if self.eFind.get():
			field.tag_remove("find", 1.0, END)# reset found text tag
			start = 1.0
			while 1:# parse all text
				lenSearch = StringVar()
				pos = field.search(self.searchText.get(), start, stopindex="end", nocase=abs(self.caseSensitiveStatus.get()-1), regexp=self.regexStatus.get(), count=lenSearch)
				if not pos:break # stop do find/replace
				field.see(pos) # need to scroll to found text
				
				line, col = pos.split('.')
				findTextPos = (pos,"%s.%d"%(line, int(col)+int(lenSearch.get())))
				field.delete(findTextPos[0],findTextPos[1])# delete old text
				field.insert(findTextPos[0],self.replaceText.get())# insert replace text
				start = pos + "+"+str(len(self.replaceText.get()))+"c"
				replaceTimes += 1
			
			self.findReplaceStatus.configure(text=self.searchText.get()+" found/replaced "+str(replaceTimes)+" times", foreground="black")
		else:
			self.findReplaceStatus.configure(text="nothing to replace")
		
	def openVirtualBasic(self, event=None):# open a virtualBasic file with .baz extension or all files (put code in text files only)
		fileName = tkFileDialog.askopenfilename(parent=self.tkRoot, title="Open VirtualBasic file", initialdir=self.virtualOpenRoot, filetypes=[("VirtualBasic files","*.baz"),('all files', '.*')])
		if fileName:
			self.backupField("virtualBasic")
			self.virtualOpenRoot = path.dirname(fileName) # set new root directory for ask dialog
			# read virtualBasic source file
			try:
				# f = codecs.open(fileName, mode='r', encoding='utf-8', errors='ignore')# in case of charset dificulty
				f = open(fileName, mode='r')
				lines = f.readlines()
				f.close()
				content = "\n".join(self.cleanLines(lines))
				content.decode("utf-8")# check if utf-8 charset
				self.addFileToBuffers(field="virtualBasic", file=fileName, content=content)
				self.updateFieldInfo(field="virtualBasic", file=fileName, size=str(getsizeof(content)))
				self.fieldVirtual.delete(0.1, END)
				self.fieldVirtual.insert(INSERT, content)
				self.fieldFocus = "virtualBasic"
				self.fieldVirtual.highLightingText()
			except:
				self.status("Cannot read "+fileName+" file (no utf-8 charset ?) or file not found !", "red") 

			# self.fieldVirtual.highLightingText()# here for test only, to catch errors
		else:
			self.status("open virtualBasic File cancelled", "red")
			
		return "break" # prevent for bind->handler in Text class
			
	def openBasic(self, event=None):# open a AppleSoft Basic file with .bas extension or all files (put code in text files only)
		fileName = tkFileDialog.askopenfilename(parent=self.tkRoot, title="Open AppleSoft Basic file", initialdir=self.basicOpenRoot, filetypes=[("AppleSoft Basic files","*.bas"),('all files', '.*')])
		if fileName:
			self.backupField("appleSoftBasic")
			self.basicOpenRoot = path.dirname(fileName) # set new root directory for ask dialog
			# read AppleSoft source file
			try:
				# f = codecs.open(fileName, mode='r', encoding='utf-8', errors='ignore')# in case of charset dificulty
				f = open(fileName, mode='r')
				lines = f.readlines()
				f.close()
				content = "\n".join(self.cleanLines(lines))
				content.decode("utf-8")# check if utf-8 charset
				self.addFileToBuffers(field="appleSoftBasic", file=fileName, content=content)
				self.updateFieldInfo(field="appleSoftBasic", file=fileName, size=str(getsizeof(content)))
				self.fieldBasic.delete(0.1, END)
				self.fieldBasic.insert(INSERT, content)
				self.fieldFocus = "appleSoftBasic"
				self.fieldBasic.highLightingText()
			except:
				self.status("Cannot read file "+fileName+" (no utf-8 charset ?) or file not found !", "red")
		else:
			self.status("open AppleSoft Basic File cancelled", "red")
			
		return "break" # prevent for bind->handler in Text class
			
	def saveVirtualBasic(self, event=None):# save virtualBasic file with .baz extension or all files (put code in text files only)
		if self.fileVirtualOpened == "":
			fileName = tkFileDialog.asksaveasfilename(parent=self.tkRoot, title='Save VirtualBasic file', initialdir=self.virtualSaveRoot, filetypes=[("VirtualBasic files","*.baz"), ('all files', '.*')])
			self.fileVirtualOpened = fileName # don't forget to remember
			self.labelFileVirtual.config(text="file "+fileName)
		else:
			fileName = self.fileVirtualOpened
			
		if fileName:
			self.virtualSaveRoot = path.dirname(fileName)
			content = self.fieldVirtual.get(0.1, END)
			try:
				f = codecs.open(fileName, mode='w', encoding='utf-8', errors='ignore')
				# f = open(fileName, mode='w')
				f.write(content)
				f.close()
				self.updateFieldInfo(field="virtualBasic", file=fileName, size=str(getsizeof(content)), msg="file saved")
				self.addFileToBuffers(field="virtualBasic", file=fileName, content=content)
			except:
				self.status("can't write file"+fileName, "red")
		else:
			self.status("save virtualBasic File cancelled", "red")
			
		return "break" # prevent for bind->handler in Text class
			
	def saveVirtualBasicAsCopy(self, event=None):# save virtualBasic file copy
		fileName = tkFileDialog.asksaveasfilename(parent=self.tkRoot, title='Save VirtualBasic file as copy', initialfile=self.fileVirtualOpened, initialdir=self.virtualSaveRoot, filetypes=[("VirtualBasic files","*.baz"), ('all files', '.*')])
		
		if fileName:
			content = self.fieldVirtual.get(0.1, END)
			self.virtualSaveRoot = path.dirname(fileName)
			try:
				f = codecs.open(fileName, mode='w', encoding='utf-8', errors='ignore')
				# f = open(fileName, mode='w')
				f.write(content)
				f.close()
				self.updateFieldInfo(field="virtualBasic", file=fileName, size=str(getsizeof(content)), msg="file saved as")
				self.addFileToBuffers(field="virtualBasic", file=fileName, content=content)
			except:
				self.status( "can't write file"+fileName, "red")
		else:
			self.status( "save as virtualBasic File cancelled", "red")
				
	def saveBasic(self, event=None):# save AppleSoft Basic file with .bas extension or all files (put code in text files only)
		if self.fileBasicOpened == "":
			fileName = tkFileDialog.asksaveasfilename(parent=self.tkRoot, title='Save AppleSoft Basic file', initialdir=self.basicSaveRoot, filetypes=[("AppleSoft Basic files","*.bas"), ('all files', '.*')])
			self.fileBasicOpened = fileName# don't forget to remember
			self.labelFileBasic.config(text="file "+fileName)
		else:
			fileName = self.fileBasicOpened
			
		if fileName:
			content = self.fieldBasic.get(0.1, END)
			self.basicSaveRoot = path.dirname(fileName)
			try:
				f = codecs.open(fileName, mode='w', encoding='utf-8', errors='ignore')
				# f = open(fileName, mode='w')
				f.write(content)
				f.close()
				self.updateFieldInfo(field="appleSoftBasic", file=fileName, size=str(getsizeof(content)), msg="file saved")
				self.addFileToBuffers(field="appleSoftBasic", file=fileName, content=content)
			except:
				self.status("can't write file"+fileName, "red")
		else:
			self.status( "save AppleSoft Basic File cancelled", "red")
			
		return "break" # prevent for bind->handler in Text class
			
	def saveBasicAsCopy(self, event=None):# save AppleSoft Basic file copy
		fileName = tkFileDialog.asksaveasfilename(parent=self.tkRoot, title='Save AppleSoft Basic file as copy', initialfile=self.fileBasicOpened, initialdir=self.basicSaveRoot, filetypes=[("AppleSoft Basic files","*.bas"), ('all files', '.*')])
			
		if fileName:
			content = self.fieldBasic.get(0.1, END)
			self.basicSaveRoot = path.dirname(fileName)
			try:
				f = codecs.open(fileName, mode='w', encoding='utf-8', errors='ignore')
				# f = open(fileName, mode='w')
				f.write(content)
				f.close()
				self.updateFieldInfo(field="appleSoftBasic", file=fileName, size=str(getsizeof(content)), msg="file saved as")
				self.addFileToBuffers(field="appleSoftBasic", file=fileName, content=content)
			except:
				self.status("can't write file"+fileName, "red")
		else:
			self.status( "save as AppleSoft Basic File cancelled", "red")
			
	def openAppleWin(self, event=None):
		# open image
		try:
			Popen([appleWin])
		except:
			self.status("can't open "+appleWin, "red")
			return 0
			
	def drawInterface(self):
		# set menu
		menubar = Menu(self.tkRoot)
		# file
		mFile = Menu(menubar, tearoff=0)
		menubar.add_cascade(label="File", menu=mFile)
		mFile.add_command(label="Open VirtualBasic File", command=self.openVirtualBasic, accelerator="Ctrl+O")
		mFile.add_command(label="Save VirtualBasic File", command=self.saveVirtualBasic, accelerator="Ctrl+S")
		mFile.add_command(label="Save VirtualBasic File as copy", command=self.saveVirtualBasicAsCopy)
		mFile.add_separator()
		mFile.add_command(label="Open AppleSoft Basic File", command=self.openBasic, accelerator="Ctrl+Alt+O")
		mFile.add_command(label="Save AppleSoft Basic File", command=self.saveBasic, accelerator="Ctrl+Alt+S")
		mFile.add_command(label="Save AppleSoft Basic File as copy", command=self.saveBasicAsCopy)
		mFile.add_separator()
		mFile.add_command(label="Quit", command=self.quitInterface, accelerator="Ctrl+Q")
		# convert/edit
		mConvert = Menu(menubar, tearoff=0)
		menubar.add_cascade(label="Convert/Edit", menu=mConvert)
		mConvert.add_command(label="Convert to Applesoft Basic", command=self.virtualToBasicStd, accelerator="Ctrl+B")
		mConvert.add_command(label="Convert to Applesoft Basic Light", command=self.virtualToBasicLight, accelerator="Ctrl+L")
		mConvert.add_command(label="Convert to Applesoft Basic more Commented", command=self.virtualToBasicRemgo)
		mConvert.add_command(label="Convert to Applesoft Basic Compacted", command=self.virtualToBasicCompacted, accelerator="Ctrl+Alt-C" )
		mConvert.add_separator()
		mConvert.add_command(label="Merge/import inserts in VirtualBasic", command=self.virtualMerge)
		mConvert.add_command(label="Convert ':' to '\\n' in Field Basic", command=self.splitDbPointBasic)
		mConvert.add_separator()
		mConvert.add_command(label="Convert to VirtualBasic", command=self.basicToVirtual)
		mConvert.add_command(label="Compact Basic Code", command=self.compactBasic)
		mConvert.add_separator()
		mConvert.add_command(label="Define line number step", command=self.askLineNumberStep)
		mConvert.add_separator()
		mConvert.add_command(label="Refresh syntax highlighting", command=self.refreshHighLighting, accelerator="Ctrl+R")
		mConvert.add_separator()
		mConvert.add_command(label="Find and Replace", command=self.findAndReplaceWin, accelerator="Ctrl+F")
		if appleWin : mConvert.add_command(label="Open AppleWin", command=self.openAppleWin)
		mConvert.add_command(label="Undo", command=self.undoLastEdit, accelerator="Ctrl+U")
		# root dir
		mRootDir = Menu(menubar, tearoff=0)
		menubar.add_cascade(label="Root Dir", menu=mRootDir)
		mRootDir.add_command(label="Change root directory", command=self.changeVirtualRootProject, accelerator="Ctrl+D")
		# zoom text and enhance field virtualbasic
		mZoom = Menu(menubar, tearoff=0)
		menubar.add_cascade(label="Zoom", menu=mZoom)
		mZoom.add_command(label="Font Size +", command=self.zoomTextIn, accelerator="Ctrl+Up")
		mZoom.add_command(label="Font Size -", command=self.zoomTextOut, accelerator="Ctrl+Down")
		mZoom.add_command(label="Enlarge Field +", command=self.fieldVirtualShrink, accelerator="Ctrl+Right")
		mZoom.add_command(label="Reduce Field -", command=self.fieldVirtualEnlarge, accelerator="Ctrl+Left")
		# select
		mSelect = Menu(menubar, tearoff=0)
		menubar.add_cascade(label="Select/Copy", menu=mSelect)
		mSelect.add_command(label="Select VirtualBasic", command=self.selectallVirtual)
		mSelect.add_command(label="Select AppleSoft Basic", command=self.selectallBasic)
		mSelect.add_command(label="Copy AppleSoft Basic", command=self.copyBasic)
		# buffers
		self.mBuffers = Menu(menubar, tearoff=0)
		menubar.add_cascade(label="Buffers", menu=self.mBuffers)
		self.mBuffers.add_command(label="Close last buffer", command=self.deleteBufferEntry)
		self.mBuffers.add_separator()
		# about
		mAbout = Menu(menubar, tearoff=0)
		menubar.add_cascade(label="About", menu=mAbout)
		mAbout.add_command(label="Author", command=self.about)
		mAbout.add_command(label="Virtual Basic Editor Infos", command=self.virtualBasicEditorInfos)
		mAbout.add_command(label="Code Stats", command=self.statsOnCode)
		# menuAddons
		try:
			if menuAddons:
				mAddons = Menu(menubar, tearoff=0)
				menubar.add_cascade(label="Addons", menu=mAddons)
				for menuEntry in menuAddons:
					eval(menuEntry)
		except:
			pass

		# set menu
		self.tkRoot.configure(menu=menubar)
		
		# set labels, scrollbars and fields
		# field virtualbasic
		labelVirtual = Label(self.tkRoot, text=" Virtual Basic ", font=self.customFontBold, background="#CCCCCC", relief=GROOVE)
		self.labelFileVirtual = Label(self.tkRoot, font=self.customFont, text="file "+self.fileVirtualOpened)
		self.fieldVirtual = SyntaxHighlightingText(self.tkRoot)
		self.fieldVirtual.config(wrap=WORD, font=self.customFont, width=80, height=40, relief=SUNKEN, undo=True, background="white")
		self.fieldVirtual.bind("<FocusIn>", self.focusInFieldVirtual)
		self.fieldVirtual.bind("<ButtonRelease-1>", self.getSelection)
		self.fieldVirtual.bind("<Control-a>", self.selectallVirtual)
		self.fieldVirtual.focus_set()
		
		scrollbarV = Scrollbar(self.tkRoot)
		scrollbarV.config(command=self.fieldVirtual.yview)
		self.fieldVirtual.config(yscrollcommand=scrollbarV.set)
		
		# fields basic
		labelBasic = Label(self.tkRoot, text=" AppleSoft Basic ", font=self.customFontBold, background="#CCCCCC", relief=GROOVE)
		self.labelFileBasic = Label(self.tkRoot, font=self.customFont, text="file "+self.fileBasicOpened)
		self.fieldBasic = SyntaxHighlightingText(self.tkRoot)
		self.fieldBasic.config(wrap=WORD, font=self.customFont, width=80, height=40, relief=SUNKEN, undo=True, background="white")
		self.fieldBasic.bind("<FocusIn>", self.focusInFieldBasic)
		self.fieldBasic.bind("<ButtonRelease-1>", self.getSelection)
		self.fieldBasic.bind("<Control-a>", self.selectallBasic)
	
		scrollbarB = Scrollbar(self.tkRoot)
		scrollbarB.config(command=self.fieldBasic.yview)
		self.fieldBasic.config(yscrollcommand=scrollbarB.set)
		
		# label root dir
		self.labelRootVirtual = Label(self.tkRoot, font=self.customFont, text="Root project "+self.root)
		
		# label status
		self.labelStatus = Label(self.tkRoot, font=self.customFont, text="status")
		
		# main shortcuts
		mainShortcuts = {
			"<Control-q>":		self.quitInterface,
			"<Control-d>":		self.changeVirtualRootProject,
			"<Control-b>":		self.virtualToBasicStd,
			"<Control-Alt-b>":	self.virtualToBasicLoz,# hidden shortcut only for me
			"<Control-l>":		self.virtualToBasicLight,
			"<Control-Alt-c>":	self.virtualToBasicCompacted,
			"<Control-u>":		self.undoLastEdit,
			"<Control-Up>":		self.zoomTextIn,
			"<Control-Down>":	self.zoomTextOut,
			"<Control-Left>":	self.fieldVirtualEnlarge,
			"<Control-Right>":	self.fieldVirtualShrink,
			"<Control-f>":		self.findAndReplaceWin,
			"<Control-o>":		self.openVirtualBasic,
			"<Control-s>":		self.saveVirtualBasic,
			"<Control-Alt-o>":	self.openBasic,
			"<Control-Alt-s>":	self.saveBasic,
			"<Control-r>":		self.refreshHighLighting
		}
		
		# set shortcuts
		for event, handler in mainShortcuts.items():
			# bind field is better than bind_all
			self.fieldVirtual.bind(event, handler)
			self.fieldBasic.bind(event, handler)
		
		# grid
		rootGrid = [
			[labelVirtual, 					0, 0, 1, 2, W+E+N+S],
			[labelBasic, 					0, 2, 1, 2, W+E+N+S],
			[self.labelFileVirtual, 		1, 0, 1, 2, W],
			[self.labelFileBasic, 			1, 2, 1, 2, W],
			[self.fieldVirtual, 			2, 0, 1, 1, W+E+N+S],
			[scrollbarV, 					2, 1, 1, 1, N+S],
			[self.fieldBasic, 				2, 2, 1, 1, W+E+N+S],
			[scrollbarB, 					2, 3, 1, 1, N+S],
			[self.labelRootVirtual, 		3, 0, 1, 4, W],
			[self.labelStatus, 				4, 0, 1, 4, W]
		]
		for w in rootGrid:
			w[0].grid(row=w[1], column=w[2], rowspan=w[3], columnspan=w[4], sticky=w[5])
			
			
		# geometry configure, resize fields when resize window
		self.tkRoot.grid_columnconfigure(0, weight=1)
		self.tkRoot.grid_columnconfigure(2, weight=1)
		self.tkRoot.grid_rowconfigure(2, weight=1)
		return 1

class SyntaxHighlightingText(Text):# syntax highlighting code
	"""Class SyntaxHighlightingText highlight text with virtualBasic and Applesoft Basic syntax 0.0.8 2015"""
	def __init__(self, root):
		Text.__init__(self, root)
		self.tagColors = {
			"keyword":("blue",None), "int":("red",None), "comment":("orange red",None),
			"link":("dark green",None), "braces":("magenta",None), "rem":("grey40",None),
			"default":("black",None),"tabspace":("black","white"),"find":("yellow","dark blue")
		}
		self.basicKeyWords = [
			'ABS', 'AND', 'APPEND', 'ASC', 'AT', 'ATN', 'BLOAD', 'BRUN', 'BSAVE', 'CALL', 'CHAIN', 'CHR', 'CLEAR', 'CLOSE', 'CLOSESECTION', 'COLOR', 'CONT', 'COS', 'DATA', 'DEF', 'DIM', 'DRAW', 'END', 'EXEC', 'EXP', 'FLASH', 'FN', 'FOR', 'FP', 'FRE', 'GET', 'GOSUB', 'GOTO', 'GR', 'HCOLOR', 'HGR', 'HGR2', 'HIMEM', 'HLIN', 'HOME', 'HPLOT', 'HTAB', 'IF', 'IN', 'INIT', 'INPUT', 'INT', 'INVERSE', 'LEFT', 'LEN', 'LET', 'LIST', 'LOAD', 'LOCK', 'LOG', 'LOMEM', 'MAXFILES', 'MID', 'MON', 'NEW', 'NEXT', 'NOMON', 'NORMAL', 'NOT', 'NOTRACE', 'ON', 'ONERR', 'OPEN', 'OR', 'PDL', 'PEEK', 'PLOT', 'POKE', 'POP', 'POSITION', 'PR', 'PRINT', 'READ', 'RECALL', 'REM', 'RENAME', 'RESTORE', 'RESUME', 'RETURN', 'RIGHT', 'RND', 'ROT', 'RUN', 'RUN', 'SAVE', 'SCALE', 'SCRN', 'SECTION', 'SGN', 'SHLOAD', 'SIN', 'SPC', 'SPEED', 'SQR', 'STEP', 'STOP', 'STORE', 'STR', 'TAB', 'TAN', 'TEXT', 'THEN', 'TO', 'TRACE', 'UNLOCK', 'USR', 'VAL', 'VERIFY', 'VLIN', 'VTAB', 'WAIT', 'WRITE', 'XDRAW', 'XPLOT'
		]
		self.config_tags()
		# self.bind("<Leave>", self.highLightingText)# only when leave the widget, very slow with long code
		
	def config_tags(self):
		for tag, colors in self.tagColors.items():
			foreground, background = colors
			self.tag_config(tag, foreground=foreground, background=background)
		
	def highLightingText(self, event=None): # highLight text with virtualBasic and Applesoft Basic syntax
		for tag in self.tag_names(index=None):
			self.tag_remove(tag, 1.0, END) # remove all tags
		
		for basicKeyWord in self.basicKeyWords:
			start = 1.0
			while 1:# parse all keywords before other stuff
				pos = self.search(r'\y%s\y' % basicKeyWord, start, stopindex="end", nocase=1, regexp=1)
				if not pos:break
				line, col = pos.split('.')
				self.tag_add("keyword", pos, "%s.%d" % (line, int(col)+len(basicKeyWord)))
				start = pos + "+"+str(len(basicKeyWord))+"c"
		
		# not keywords only regex, list instead dict to preserve order
		patterns = [
			(r'\?',										"keyword"),							
			(r'\y[0-9]+\y',								"int"),
			(r'@[A-Z]+',								"link"),
			(r'£[A-Z]+',								"link"),
			(r'===[a-zA-Z0-9\/\-\_\s]+\.ba[sz]===',		"link"),
			(r'\".*?\"',								"default"),
			(r'\'.*?\'',								"default"),
			(r'\{.+?\}',								"braces"),
			(r'^\s+',									"tabspace"),
			(r'section.*$',								"comment"),
			(r'closesection.*$',						"comment"),
			(r'rem.*$',									"rem"),
			(r'#.*$',									"comment") # last position to hide all tags if match comment line
		]
		
		for regex, tag in patterns: # preserve tags order
			self.tag_raise(tag) # replace tag_remove
			self.highLightingRegex(regex=regex, tag=tag)
			
		self.tag_raise("find") # place to the top
			
	def highLightingRegex(self, regex=None, tag=None):# highLight text with regex only
		start = 1.0	
		while 1:
			txtLength = IntVar()
			pos = self.search(regex, start, stopindex="end", nocase=1, regexp=1, count=txtLength)
			if not pos:break
			line, col = pos.split('.')
			
			#for tagToDel in self.tag_names(index=None):# clean all tags in this regex
				#self.tag_remove(tagToDel, pos, "%s.%d" % (line, int(col)+txtLength.get()))
				
			self.tag_add(tag, pos, "%s.%d" % (line, int(col)+txtLength.get()))
			start = pos+"+"+str(txtLength.get())+"c"

class Vbe(virtualBasicEditor):
	def __init__(self):
		virtualBasicEditor.__init__(self)
		
if __name__ == "__main__":
	print NAME,VERSION
	print virtualBasicEditor.__doc__
	print SyntaxHighlightingText.__doc__
	print "\n".join([AUTHOR,COPYRIGHT,URL])
	o = virtualBasicEditor()
	del o
