#! /usr/bin/env python
# *-* coding: utf-8 *-*
from os import listdir, path, rename, remove
from Tkinter import *
import tkFileDialog
import shutil
import codecs
import re

NAME = "Ascii to utf-8 converter"
VERSION = "0.0.1 2015"
AUTHOR = "Andres Lozano a.k.a Loz"
COPYRIGHT = "Copyleft: This is a free work, you can copy, distribute, and modify it under the terms of the Free Art License http://artlibre.org/licence/lal/en/"
URL = "http://virtualbasic.org"

class ListDirectory():
	# code to list files
	# create a list directory var "filesList" filtered by param "extensions" of folder "root"
	# need from os import listdir, path
	def __init__(self, root="", extensions=[]):
		self.filesList = []
		self.extensions = extensions
		if root:self.list_dir(root)
		
	def list_dir(self,d):
		for _path in (path.join(d,f) for f in listdir(d)):
			if path.isdir(_path):
				self.list_dir(_path)
			else:
				extention = _path[-3:].lower() 
				if extention in self.extensions:
					self.filesList.append(_path)
					
		self.filesList.sort()
		
class ConvertUtf(ListDirectory):
	"""Class ConvertUtf 0.0.1"""
	def __init__(self, folder="", file="", extensions=["baz","bas"]):
		ListDirectory.__init__(self, root=folder, extensions=extensions)
		if file:self.filesList = [file]
		
	def toUtf8(self):
		for file in self.filesList:
			next = 1
			lines = self.getFileContent(fileName=file)
			if type(lines).__name__ == "list":
				lines = self.replacePoundChar(lines)
			else:
				print "get file "+lines
				next = 0
				
			if type(lines).__name__ == "list" and next:	
				lines = self.encodeText(lines)
			else:
				print "replace "+lines
				next = 0
				
			if type(lines).__name__ == "list" and next:
				lines = self.re_writeFile(fileName=file,lines=lines)
			else:
				print "encode "+lines
	
	def re_writeFile(self,fileName="",lines=[]):
		content = "\n".join(self.cleanLines(lines=lines))
		if content:
			try:
				remove(fileName)
				f = codecs.open(fileName, mode='w', encoding='utf-8', errors='ignore')
				f.write(content)
				f.close()
				remove(fileName+".bak")
				print fileName+" ok"
				return []
			except:
				rename(fileName+".bak",fileName)
				print "*** can't white utf-8 "+fileName
				return "error"
				
	def encodeText(self, lines):
		regex = re.compile("&")
		result = []
		try:
			for line in lines:
				line = "".join([x for x in line if ord(x) < 128])# only ascii chars
				result.append(regex.sub(u'\xa3',line.encode("utf-8"))) # convert raw byte & char to utf-8
			return result
		except:
			return "error"
		
	def replacePoundChar(self,lines=[]):
		pound = re.compile('\xa3')
		result = []
		for line in lines:
			result.append(pound.sub("&",line))
		return result
		
	def getFileContent(self,fileName=""):
		try:
			filedata = codecs.open(fileName, mode='r', encoding='UTF-8').read() 
		except:
			f = open(fileName,"r")
			lines = f.readlines()
			f.close()
			print fileName+" read"
			shutil.copy2(fileName,fileName+".bak")
			return lines
		
		print "*** can't open "+fileName
		return "error"
	
	def cleanLines(self,lines=[]):# rip all end lines
		result = []
		s= ""
		crln = re.compile("\r|\n")
		for line in lines:
			s = crln.sub("",line)
			result.append(s)
		return result
		
def convertFiles2utf8(mod="file"):
	# convert to utf-8 all files bas and baz files in folder
	if mod == "folder":
		win = Tk()
		folder = tkFileDialog.askdirectory(parent=win, title='Select folder')
		win.destroy()
		if not folder:return 0
		print "convert "+folder
		o = ConvertUtf(folder=folder)
		o.toUtf8()
		del o
		return 1
	else:
		win = Tk()
		file = tkFileDialog.askopenfilename(parent=win, title='Select file')
		win.destroy()
		if not file:return 0
		print "convert "+file
		o = ConvertUtf(file=file)
		o.toUtf8()
		del o
		return 1
			
if __name__ == "__main__":
	print NAME
	print ConvertUtf.__doc__
	print "\n".join([AUTHOR,COPYRIGHT,URL])
	print "convert file or directory, baz and baz extensions files"
	print 
	if raw_input("directory d, file f: ") == "d":
		dir = 1
	else:
		dir = None
	
	while 1:
		if dir:
			convertFiles2utf8(mod="folder")
		else:
			convertFiles2utf8(mod="file")
			
		if raw_input("return to continue, q to quit: ") == "q":
			break
	