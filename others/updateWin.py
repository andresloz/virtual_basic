#!/usr/bin/env python
#*- coding: utf-8 -*-

NAME = "Update files"
VERSION = "0.0.1 2015"
AUTHOR = "Andres Lozano a.k.a Loz"
COPYRIGHT = "Copyleft: This is a free work, you can copy, distribute, and modify it under the terms of the Free Art License http://artlibre.org/licence/lal/en/"
URL = "http://virtualbasic.org"

import shutil
import os

dest1 = "C:/Python27/Lib/site-packages/virtualBasicEditor/"
dest2 = "C:/Python27/Lib/site-packages/virtualbasic/"
dest3 = "C:/Users/loz/Desktop/loz_code/py_virtualbasic/virtualbasic-code-w/virtualbasic/"

files = [
	["../virtualBasicEditor/__init__.py",						dest1],
	["../virtualBasicEditor/virtualBasicEditor.py", 			dest1],
	["../virtualBasicEditor/virtualbasic/__init__.py", 			dest2],
	["../virtualBasicEditor/virtualbasic/virtualbasic.py", 		dest2],
	["../virtualBasicEditor/virtualbasic/__init__.py", 			dest3],
	["../virtualBasicEditor/virtualbasic/virtualbasic.py", 		dest3]
]

def createDirsIfNotExist(dirs=[]):
	for directory in dirs:
		if not os.path.isdir(directory):
			os.mkdir(directory)
			
def copyFile(src, dest):
	try:
		shutil.copy(src, dest)
		print "file", src, "copied to", dest
	# eg. src and dest are the same file
	except shutil.Error as e:
		print('Error: %s' % e)
	# eg. source or destination doesn't exist
	except IOError as e:
		print('Error: %s' % e.strerror)

if __name__ == "__main__":
	print NAME,VERSION
	print "\n".join([AUTHOR,COPYRIGHT,URL])
	print
	# program
	createDirsIfNotExist([dest1,dest2])
	for e in files:
		src, dest = e
		srcTime = os.stat(src)[8]
		try:
			destTime = os.stat(dest+os.path.basename(src))[8]
		except:
			destTime = 0
			
		if srcTime > destTime:
			copyFile(src,dest)
			
	print "update done"
