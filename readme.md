Virtual Basic for Applesoft Basic
==================================

Virtual Basic Editor version 0.0.9 and virtualbasic version 0.3.5
--------------------------------

Virtual Basic for Applesoft Basic

With Virtual Basic rules and some Python scripts
we can improve the way to code Applesoft Basic.

No more numered lines, you code like contemporary langages.

Learn Virtual Basic rules by opening the example.baz file (with Virtual Basic Editor) and reading the "HOW TO"... and test it !

Or visit the web site [virtualbasic.org](http://virtualbasic.org)

Execute virtualBasicEditor.pyw and use a graphical user interface to edit your code, 
save and convert in many ways your virtualBasic code to regular Applesoft Basic.

Or execute in command line virtualbasic.py script and follow instructions to convert Virtualbasic files <=> Applesoft Basic files 
or use "from virtualbasic import *" to integrate python VirtualBasic classes to your own python code and easily manipulate basic.

virtualbasic.py and virtualBasicEditor.pyw scripts work fine with python 2.7.x and use utf-8 charset

How to use Virtual Basic ?
-------------------------

Download all the python files, examples and directories of this repository

Or download only virtualBasicEditor.pyw and virtualBasicEditor folder in the same directory

Virtual Basic method 1 ?
------------------------

1. Execute virtualBasicEditor.pyw and explore graphical user interface
2. Write your virtualBasic code and convert it into Basic
3. Select the Basic code and paste it into emulator an run it
4. Save both codes if you want

Virtual Basic method 2 (command line) ?
------------------------

1. Write your VirtualBasic code in a file (text mode with .baz or .txt extension)
2. Execute virtualbasic.py and choose 1
3. virtualbasic.py create a new file with .bas extension
4. Open the new file and check it
5. Paste the code of the new file into emulator an run it

In this directory there are examples of Virtual Basic code
----------------------------------------------------------

1. example.baz which contains the "HOW TO"
2. x.baz an example of files browser
3. clock.baz an example graphic clock
4. lib directory contains include files for basic examples scripts

Andres Lozano a.k.a Loz, 2012-2015

A "big thanks you" to Guillaume Courtier

Copyleft: this work is free, you can copy, distribute and modify it under the terms of the Free Art License http://www.artlibre.org
